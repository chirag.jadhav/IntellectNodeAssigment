var express = require('express');
var router = express.Router();
var ObjectID = require('mongodb').ObjectID;
var moment = require('moment');
var async = require('async');

router.post('/addUser',function(req,res){
    var db = req.app.locals.db;
	var collection = db.collection('users');
	var user = {
                    fName: req.body.fName,
                    lName : req.body.lName,
                    email : req.body.email,
                    pinCode : req.body.pinCode,
					birthDate : new Date(req.body.birthDate),
					isActive : req.body.isActive
                };
                collection.insertOne(user, function(err, result){
                    if(err){
                        console.log(err);
						res.status(400).json({msg : 'Error While Inserting Users '});
                    }else{
                        res.status(200).json(result);
                    }
                });
});

router.get('/getUserDetails/:id', function(req,res){
	var db = req.app.locals.db;
	var collection = db.collection('users');
	collection.find({_id : new ObjectID(req.params.id)}).toArray( function(err, results){
		if(err){
			console.log(err);
			res.status(400).json({msg : 'Something Went wrong'});
		}else{
			console.log(results)
			if(results.length > 0){
				res.status(200).json(results);	
			}else{
				res.status(400).json({msg : "No Such User Exist's"});
			}
		}
	});
})


router.post('/toDos',function(req,res){
    var db = req.app.locals.db;
	var collection = db.collection('toDos');
	var user = {
                    userid: ObjectID(req.body.userid),
                    text : req.body.text,
                    done : req.body.done,
                    targetDate : new Date(req.body.targetDate),
                };
                collection.insertOne(user, function(err, result){
                    if(err){
                        console.log(err);
						res.status(400).json({msg : 'Error While Inserting To Dos '});
                    }else{
                        res.status(200).json(result);
                    }
                });
});

router.get('/getUsertoDos/:id', function(req,res){
	var db = req.app.locals.db;
	var collection = db.collection('toDos');
	collection.find({_id : new ObjectID(req.params.id)}).toArray( function(err, results){
		if(err){
			console.log(err);
			res.status(400).json({msg : 'Something Went wrong'});
		}else{
			console.log(results)
			if(results.length > 0){
				res.status(200).json(results);	
			}else{
				res.status(400).json({msg : "No Such To Dos Exist's"});
			}
		}
		
		
	});
	
});

router.get('/allActiveUsersWithToDos', function(req, res){
	var db = req.app.locals.db;
	var collection = db.collection('users');
	var users = [];
	collection.find({ "isActive": "true"}).toArray( function(err, results){
		if(err){
			console.log(err);
			res.status(400).json({msg : 'Something Went wrong'});
		}else{
			console.log('No of users -- > ', results.length)
			async.each(results, function(user, callback){
				var db = req.app.locals.db;
				var collection = db.collection('toDos');
				console.log(user._id)
				collection.find({userid : new ObjectID(user._id)}).toArray( function(err, results){
					if(err){
						console.log(err);
						callback(err);
					}else{
						user.toDos = results;
						users.push(user);
						callback(null);	
					}
				});		  
			},function(err){
				if(err){
					console.log(err);
					res.status(400).json({msg : 'Error while fetching data !!'});
				}else{
				 res.status(200).json(results);	
				}
			}
		)	
		}
		
	});
	
	
});




router.get('/allActiveTodos/:userId', function(req, res){
	var db = req.app.locals.db;
	var collection = db.collection('toDos');
	collection.find({userid : new ObjectID(req.params.userId), done : "false", targetDate : { $gte : new Date()  , $lte : new Date(moment().add(1, 'days')) }})
		.toArray( function(err, results){
					if(err){
						console.log(err);
						callback(err);
					}else{
						res.status(200).json(results)	
					}
				});		  			
	
})

module.exports = router;
