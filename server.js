var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');
var path = require('path');
var fs = require('fs');
var MongoClient = require('mongodb').MongoClient;

var user = require('./api/user');

var app = express();

app.set('port', process.env.PORT || 3001);
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(function(req,res,next){
	next();
});

app.use('/DB',express.static(path.join(__dirname, 'DB')));

app.use('/api/user', user);

var url = "mongodb://root:root@ds139585.mlab.com:39585/assigment"

MongoClient.connect(url, function(err, db){
	if(err){
		console.log(err);
	}else{
		console.log('Connected to Mongo Srever');
		app.locals.db = db;
		//global.mongoDb = db ;
		app.listen(app.get('port'), function(){
			console.info('Node.js app is listening at localhost:'+ app.get('port'));
		});
	}
});

module.exports = app;